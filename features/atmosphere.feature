Feature: Text presence

    Background:
        Given browser "Firefox"

    Scenario: Create an instance
        When I go to Jetstream
        Then I should see and press "Login with XSEDE" within 10 seconds
        Then I should see and press "Continue" within 5 seconds
        Then I should see "Welcome to the XSEDE's Client Authorization Page"
        And I login to Jetstream
        When I press the element with xpath "/html/body/div[3]/form/table/tbody/tr/td[2]/table/tbody/tr[5]/td[1]/input"
        Then I wait for 10 seconds
        And I should see and press "Projects" within 10 seconds
        Then I create project "BDD-Test" if necessary
        Then I should see and press "Dashboard" within 10 seconds
        Then I wait for 30 seconds

      #  And I wait for 30 seconds

