# Cloud UI benchmarks and integration tests

## Setup

### 1. Install geckodriver

On MacOS:

```bash
brew install geckodriver 
export PATH=/Applications/Firefox.app/Contents/MacOS:$PATH
```

On Linux:

- Download geckodriver from [this page](https://github.com/mozilla/geckodriver/releases), the latest linux64.tar.gz
- Un-archive it (e.g. `tar -xvzf geckodriver-v0.28.0-linux64.tar.gz`)
- Make the binary executable (e.g. `chmod +x geckodriver`)
- Move the binary to a directory in your system path (e.g. `mv geckodriver /usr/local/bin/`)

### 2. Install Python dependencies

Do this in a Python virtual environment:

```bash
python3 -m pip install --requirement requirements.txt 
```

## Usage

### Exosphere integration tests

Set your TACC credentials as environment variables:

```bash
read -p "Enter your TACC username: " taccusername
read -p "Enter your TACC password: " -s taccpass
export taccusername
export taccpass
```

Run all the Exosphere scenarios in order: 

```bash
behave features/exosphere.feature
```

Run selected Exosphere scenarios by tag:

```bash
behave --tags @setup features/exosphere.feature
behave --tags @launch features/exosphere.feature
behave --tags @delete features/exosphere.feature
behave --tags @cleanup features/exosphere.feature
```

Note: All the scenarios (except for the first one - `@setup`) depend on a file `/tmp/exosphere-save`. This file should contain valid local storage data for the Exosphere app. The first (`@setup`) scenario creates this file, and the last (`@cleanup`) scenario deletes it.   

You can use a custom URL for Exosphere:

```bash
behave -D EXOSPHERE_BASE_URL=http://app.exosphere.localhost:8000 features/exosphere.feature 
```

### Atmosphere benchmarks

Set Jetstream URL and credentials as environment variables:

```bash
read -p "Enter your Atmosphere username: " ATMOUSER
read -p "Enter your Atmosphere password: " -s ATMOPASS
export ATMOUSER
export ATMOPASS
export ATMOURL=https://use.jetstream-cloud.org/
```

Run all the Atmosphere scenarios in order: 

```bash
behave features/atmosphere.feature
```

### Horizon benchmarks

Set Horizon URL and credentials as environment variables:

```bash
read -p "Enter your TACC username: " HORIZONUSER
read -p "Enter your TACC password: " -s HORIZONPASS
export HORIZONUSER
export HORIZONPASS
export HORIZON_IUURL=https://iu.jetstream-cloud.org/
```

Run all the Horizon scenarios in order: 

```bash
behave features/horizon.feature
```
